﻿#if BOOTSTRAP
System.Environment.CurrentDirectory <- __SOURCE_DIRECTORY__
if not (System.IO.File.Exists "paket.exe") then let url = "https://github.com/fsprojects/Paket/releases/download/3.13.3/paket.exe" in use wc = new System.Net.WebClient() in let tmp = System.IO.Path.GetTempFileName() in wc.DownloadFile(url, tmp); System.IO.File.Move(tmp,System.IO.Path.GetFileName url);;
#r "paket.exe"
Paket.Dependencies.Install (System.IO.File.ReadAllText "paket.dependencies")
#endif

//---------------------------------------------------------------------

#I "packages/Suave/lib/net40"
#r "packages/Suave/lib/net40/Suave.dll"

open System
open System.IO
open Suave
open Suave.Operators

let path = "log.txt"

let log (request:HttpRequest) =
    use file = new StreamWriter(path, true)
    file.WriteLine(sprintf "[=== %s" (DateTime.Now.ToString()))
    for header in request.headers do file.WriteLine(sprintf "\t%s: %s" (fst header) (snd header))
    file.WriteLine(sprintf "===]\n")

let app =
    choose [
        Filters.path "/file" >=> Files.file path
        Filters.path "/log" >=> Filters.GET >=> request (fun r -> log r; Successful.OK "OK")
        Filters.path "/" >=> Successful.OK "Go away"]

let config = 
    let port = Environment.GetEnvironmentVariable("PORT")
    let ipZero = Net.IPAddress.Parse("0.0.0.0")

    { defaultConfig with 
        logger = Logging.Loggers.saneDefaultsFor Logging.LogLevel.Verbose
        bindings = [HttpBinding.mk HTTP ipZero (uint16 port)] }

startWebServer config app